const { readFile } = require('fs/promises');

(async () => {
    const input = await readFile('input-01.txt', { encoding: "utf-8" })

    const grouped = input.split('\n').map((depth, i, array) => {
        if (i === 0) return [{ sum: 0 }]
        const prev = Number(array[i-1])
        const current = Number(depth)
        const next = Number(array[i+1])
        return {
            prev,
            current,
            next,
            sum: prev+current+next
        }
    })

    const count = grouped.reduce((acc, { sum }, i, array) => {
        const prev = array[i-1]
        const next = array[i+1]
        if (!prev || !next) return acc
        if (sum > prev.sum) acc++
        return acc
    }, 0)

    console.log('Depth window increased count:', count)
})()
