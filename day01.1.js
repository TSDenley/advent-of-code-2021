const { readFile } = require('fs/promises');

(async () => {
    const input = await readFile('input-01.txt', { encoding: "utf-8" })

    const count = input.split('\n').reduce((acc, line, i, array) => {
        if (Number(line) > Number(array[i-1])) acc++
        return acc
    }, 0)

    console.log('Depth increased count:', count)
})()
