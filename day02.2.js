const { readFile } = require('fs/promises');

(async () => {
    const pos = { hoz: 0, depth: 0, aim: 0 }
    const input = await readFile('input-02.txt', { encoding: "utf-8" })

    const directions = input.trim().split('\n').map((line) => {
        const direction = line.split(' ')
        return {
            direction: direction[0],
            value: Number(direction[1])
        }
    })

    directions.forEach(({ direction, value }) => {
        switch (direction) {
            case 'forward':
                pos.hoz = pos.hoz + value
                pos.depth = pos.depth + value * pos.aim
                break
            case 'up':
                pos.aim = pos.aim - value
                break
            case 'down':
                pos.aim = pos.aim + value
                break
        }
    })

    console.log('Multiple of position & depth:', pos.hoz * pos.depth)
})()
