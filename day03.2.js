const { readFile } = require('fs/promises');

(async () => {
    // Convert a binary string into decimal integer.
    const binToDec = n => parseInt(n, 2)

    // Find the most common character in a string.
    const commonChar = (str) => {
        const chars = {}
        str.split('').forEach(c => chars[c] = chars[c] + 1 || 1)
        return Object.keys(chars).reduce((prev, next) =>
            chars[prev] >= chars[next] ? prev : next)
    }

    // Parse input file. Split into an array of arrays, lines then digits.
    const input = await readFile('input-03.txt', { encoding: "utf-8" })
    const reports = input.trim().split('\n').map(line => {
        return line.trim().split('')
    })

    // Split the input into an object containing the input in columns.
    const reportsObj = {}
    reports[0].forEach((report, i) => {
        reports.forEach(digit => {
            reportsObj[i] = (reportsObj[i] ? reportsObj[i] : '') + digit[i]
        })
    })

    console.log(reportsObj)
})()
