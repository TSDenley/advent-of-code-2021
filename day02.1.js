const { readFile } = require('fs/promises');

(async () => {
    const pos = { hoz: 0, depth: 0 }
    const input = await readFile('input-02.txt', { encoding: "utf-8" })

    const directions = input.split('\n').map((line) => {
        const direction = line.split(' ')
        return {
            direction: direction[0],
            value: Number(direction[1])
        }
    })

    directions.forEach(({ direction, value }) => {
        switch (direction) {
            case 'forward':
                pos.hoz = pos.hoz + value
                break
            case 'up':
                pos.depth = pos.depth - value
                break
            case 'down':
                pos.depth = pos.depth + value
                break
        }
    })

    console.log(pos, 'multiple:', pos.hoz * pos.depth)
})()
