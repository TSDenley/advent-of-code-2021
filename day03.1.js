const { readFile } = require('fs/promises');

(async () => {
    // Convert a binary string into decimal integer.
    const binToDec = n => parseInt(n, 2)

    // Find the most common character in a string.
    const commonChar = (str) => {
        const chars = {}
        str.split('').forEach(c => chars[c] = chars[c] + 1 || 1)
        return Object.keys(chars).reduce((prev, next) =>
            chars[prev] >= chars[next] ? prev : next)
    }

    // Parse input file. Split into an array of arrays, lines then digits.
    const input = await readFile('input-03.txt', { encoding: "utf-8" })
    const reports = input.trim().split('\n').map(line => {
        return line.trim().split('')
    })

    // Split the input into an object containing the input in columns.
    const reportsObj = {}
    reports[0].forEach((report, i) => {
        reports.forEach(digit => {
            reportsObj[i] = (reportsObj[i] ? reportsObj[i] : '') + digit[i]
        })
    })

    // Join the most/least common digit in each column to find the 'gamma' and
    // 'epsilon' rates.
    const rates = Object.entries(reportsObj).reduce((acc, [key, value]) => {
        const common = commonChar(value)
        const uncommon = common === '0' ? '1' : '0'
        acc.gamma = acc.gamma + common
        acc.epsilon = acc.epsilon + uncommon
        return acc
    }, { gamma: '', epsilon: '' })

    // Convert the rates to decimal and multiply to find the power consumption.
    console.log(binToDec(rates.gamma) * binToDec(rates.epsilon))
})()
